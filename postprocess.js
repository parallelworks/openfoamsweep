// This scripts extracts specific result files from tarballs

var path = require('path');
var fs = require('fs');
var sys = require('sys');
var exec = require('child_process').exec;

if (process.argv[2] == null) {
    console.log('Please Provide a Directory Name.');
    process.exit();
}

var directory = process.argv[2];

var depack = true;
var postproc = false;

// ---------- INITIAL READING OF THE RESULT DIRECTORIES ---------- //
fs.readdir(directory, function(err, files) {
    if (err) {
        console.log(err);
        return;
    }
    var results = [];
    for (var i = 0; i < files.length; i++) {
        if (path.extname(files[i]) === ".tgz") {
            results.push(files[i]);
        }
    }
    console.log('There are ' + results.length + ' Result Files.');
    //var stop = 1;
    var stop = results.length;
    if (depack == true) {
        exec("rm " + directory + "/vtk -R;mkdir " + directory + "/vtk;", function(error, stdout, stderr) {
            depackResults(results, stop);
        });
    }
    else {
        if (postproc == true) {
          //  postprocessResults(results, stop);
        }
    }
});

// ---------- DEPACKING OF SPECIFIC RESULT FILES FROM TAR FILES ---------- //
function depackResults(results, stop) {
    function depacker(i) {
        var filename = results[i];
        var command = "mkdir "+ directory+"/vtk/"+filename.split(".")[0]+";tar -zxf " + directory + "/" + filename +" -C "+directory+"/vtk/"+filename.split(".")[0]+" --strip-components=1;";
        exec(command, function(error, stdout, stderr) {

                    console.log('Extracted ' + (i + 1));
                    if (i < stop - 1) {
                        depacker(i + 1, stop);
                    }
                    else {
                        console.log('Extraction Process Complete.')
                        console.log('Starting Post-Processing Operations.')
                        if (postproc == true) {
                            postprocessResults(results, stop);
                        }
                    }

        });
    }
    depacker(0, stop)
}
