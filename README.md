# Swift Workflow for Parametric OpenFOAM Sweeps

This package details a Swift workflow for running parametric OpenFOAM models. The workflow reads a .sweep file, blockMeshDict_template and set of tarred OpenFOAM input files. The sweep file contains the parameters and variables for simulated. The sweep file is enumerated for all combinations of defined parameters and variables.

* * *

A sample of the sweep file input is below:
```
pname                       pvals
inletLength                 10:30:5  -> NOTE: currently only inletLength varible is parametric
inletWidth                  2:10:1
outletLength                5,8,10
outletWidth                 3,6,9
```
* * *

Flags for the main.sh file are described below:

```
-sweep = parameter/variable input file
-execute = app executable at workers
-apps = directory of app binaries to send to workers
-input = directory of openfoam configuration files
-mesh = parametric mesh template file
-count = true/false - if true only count the number of combinatorial simulations, if false run simulations
-runlocal = true/false - if true run on the local compute resource, if false run using persistant-coasters
-outdir = directory name for output files
```

* * *

**RUN INSTRUCTIONS**

On the headnode, clone the OpenFOAM workflow with the following command:

```
git clone git@bitbucket.org:parallelworks/openfoamsweep.git
```

Add Swift to your path with the following command:

```
export PATH=$PATH:$PWD/apps_local/swift-0.95-RC7/bin
```

Start the headnode coaster service with the following command:

```
./coaster_tasks/startCoaster.sh
```

On worker nodes, pull the OpenFOAM docker container using the following command: 

```
docker pull mattshax/openfoam_paraview_mesa
```

On worker nodes, run the container and start the worker.pl coaster listener - ensure to replace the correct host address where specified:

```
docker run -d mattshax/openfoam_paraview_mesa /bin/sh -c 'sudo su;/core/worker.pl <ENTER HOST ADDRESS HERE>:50100 dockerized /tmp/swiftwork'
```

Check the headnode coaster stats and ensure the worker nodes are connected. You should see something similar to below:

![Sample Coaster Stats](https://bytebucket.org/parallelworks/openfoamsweep/raw/d76058364ffe035cbd80d4b2d969b6fd8cf06642/coaster_tasks/sampleStats.png)


Once workers are connected to the resource pool, on the headnode start the workflow with the following command:

```
./start.sh
```

or to run manually

```
./main.sh -sweep=parametric.sweep -execute=RunAndReduceOpenFoam.sh -apps=apps_worker -input=parametric_openfoam_inputs -mesh=blockMeshDict_template -count=false -runlocal=false -outdir=results
```

Upon run completion, results are postprocessed and ParaView VTK files are organized by modelID in the results/vtk directory.