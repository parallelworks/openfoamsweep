#!/bin/bash

bin=$(cd $(dirname $0); /bin/pwd)

#set -x
export NODE_DIR=$bin/node
export NODE_PATH=$bin/node/node_modules

# Check if directories accessible
echo setenv.sh: FOAM_APP=$FOAM_APP
echo setenv.sh: NODE_DIR=$NODE_DIR

if [ ! -x $FOAM_APP ]; then
  echo setenv.sh: ERROR: $FOAM_APP not found or not accessible.
  RC=1
fi

if [ ! -x $NODE_DIR ]; then
  echo setenv.sh: ERROR: $NODE_DIR not found or not accessible.
  RC=1
fi

if [ _RC = _1 ]; then
  exit 1
fi
