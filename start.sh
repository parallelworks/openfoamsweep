#!/bin/bash

./main.sh -sweep=parametric.sweep -execute=RunAndReduceOpenFoam.sh -apps=apps_worker -input=parametric_openfoam_inputs -mesh=blockMeshDict_template -count=false -runlocal=false -outdir=results