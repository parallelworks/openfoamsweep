#!/bin/bash

#set -x
export ENERGYPLUS_DIR=../apps/EnergyPlus-8-1-0
export NODE_DIR=../apps/node/bin
export NODE_PATH=../apps/node/node_modules

export PATH=$PATH:/home/som/swift94/bin

# Check if directories accessible

echo setenv.sh: ENERGYPLUS_DIR=$ENERGYPLUS_DIR
echo setenv.sh: NODE_DIR=$NODE_DIR

if [ ! -x $ENERGYPLUS_DIR ]; then
  echo setenv.sh: ERROR: $ENERGYPLUS_DIR not found or not accessible.
  RC=1
fi

if [ ! -x $NODE_DIR ]; then
  echo setenv.sh: ERROR: $NODE_DIR not found or not accessible.
  RC=1
fi

if [ _RC = _1 ]; then
  exit 1
fi
