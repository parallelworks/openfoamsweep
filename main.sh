#! /bin/sh
# set -x

export SWIFT_HEAP_MAX=4G
dir=$(cd $(dirname $0); /bin/pwd)

# generate the swift apps file
cat >app <<END
persistent-coasters   sh         /bin/sh
localhost   shlocal         /bin/sh
localhost RunOFlocal $PWD/apps_worker/RunAndReduceOpenFoam.sh null null GLOBUS::maxwalltime="03:50:00"
END

export SWIFT_USERHOME=$dir/swifthome

# Generate the swift sites file
cat >sites.xml <<END
<?xml version="1.0" encoding="UTF-8"?>
<config xmlns="http://www.ci.uchicago.edu/swift/SwiftSites">

  <pool handle="cluster">
    <execution provider="coaster" jobmanager="local:pbs" />
    <profile namespace="globus" key="project">CI-SES000178</profile>
    <profile namespace="globus" key="jobsPerNode">24</profile>
    <profile namespace="globus" key="lowOverAllocation">100</profile>
    <profile namespace="globus" key="highOverAllocation">100</profile>
    <profile namespace="globus" key="providerAttributes">pbs.aprun;pbs.mpp;depth=24</profile>
    <profile namespace="globus" key="maxtime">1800</profile>
    <profile namespace="globus" key="maxWalltime">00:25:00</profile>
    <profile namespace="globus" key="userHomeOverride">$dir/swifthome</profile>
    <profile namespace="globus" key="slots">1</profile>
    <profile namespace="globus" key="maxnodes">1</profile>
    <profile namespace="globus" key="nodeGranularity">1</profile>
    <profile namespace="karajan" key="jobThrottle">1.00</profile>
    <profile namespace="karajan" key="initialScore">10000</profile>
    <profile namespace="karajan" key="workerLoggingLevel">trace</profile>
    <workdirectory>$dir/swiftwork</workdirectory>
  </pool>

  <pool handle="persistent-coasters">
    <execution provider="coaster-persistent"
               url="http://localhost:50200"
               jobmanager="local:local"/>
    <profile namespace="globus" key="workerManager">passive</profile>
    <profile namespace="globus" key="jobsPerNode">12</profile>
    <profile key="jobThrottle" namespace="karajan">2.15999</profile>
    <profile namespace="karajan" key="initialScore">10000</profile>
    <filesystem provider="local" url="none" />
    <workdirectory>/home/compute/swiftwork</workdirectory>
  </pool>

  <pool handle="localhost">
    <execution provider="coaster" jobmanager="local:local"/>
    <filesystem provider="local" />
    <execution provider="local" />
    <workdirectory>$dir/swiftwork</workdirectory>
    <profile namespace="karajan" key="initialScore">10000</profile>
    <profile namespace="karajan" key="jobThrottle">0.01</profile>
  </pool>
</config>

END

# generate the swift config file
cat >cf <<END
tc.file=app
sites.file=sites.xml
use.provider.staging=true
provider.staging.pin.swiftfiles=false
wrapperlog.always.transfer=false
sitedir.keep=true
execution.retries=0
lazy.errors=false
use.wrapper.staging=false
END

# add the applications to the path
export PATH=$dir/apps_local/swift-0.95-RC7/bin:$dir/apps_local/node:$dir/apps_local/node/node_modules:$PATH

SWEEP=$*;

# extract the run parameters
usage="$0 -sweep=test.sweep -apps=apps -input=pitzDaily -mesh=blockMeshDict_template -count=false -runlocal=false -outdir=results"

echo ""; echo Running OpenFOAM Swift Workflow;echo "";

while [ $# -gt 0 ]; do
  case $1  in
    *-sweep*)  sweep=$(echo $1 | sed 's/-sweep=/\n/g'); echo Sweep: $sweep; shift ;;
    *-execute*)  execute=$(echo $1 | sed 's/-execute=/\n/g'); echo Execute: $execute; shift ;;
    *-apps*)  apps=$(echo $1 | sed 's/-apps=/\n/g'); echo Apps: $apps; shift ;;
    *-input*)  input=$(echo $1 | sed 's/-input=/\n/g'); echo Input: $input; shift ;;
    *-mesh*)  mesh=$(echo $1 | sed 's/-mesh=/\n/g'); echo Mesh: $mesh; shift ;;
    *-count*)  count=$(echo $1 | sed 's/-count=/\n/g'); echo Count: $count; shift ;;
    *-runlocal*)  runlocal=$(echo $1 | sed 's/-runlocal=/\n/g'); echo RunLocal: $runlocal; shift ;;
    *-outdir*)  outdir=$(echo $1 | sed 's/-outdir=/\n/g'); echo Outdir: $outdir; shift ;;
    *) echo "no match"; shift ;;
  esac
done


# generate the expanded sweep file
node utils/presweep.js $sweep;SWEEPNEW=$(echo $SWEEP | sed 's/.sweep/_run.sweep/2')

# tar the app and openfoam input file directories
echo "";echo Compressing App and Input Files;
tar czf $apps.tgz $apps; tar czf $input.tgz $input
echo Compression Complete;echo "";

# run the openfoam sweep via swift 
swift -ui http:3018 -config cf openfoamsweep.swift $SWEEPNEW

# run the postprocess operation to extract VTK files
node postprocess.js $outdir

# move the vtk files to the paraview data_library
#DATE=$(date +%s)
#mv results/vtk /core/working_files/openfoam/data_library/openfoam_$DATE

./utils/clean.sh

