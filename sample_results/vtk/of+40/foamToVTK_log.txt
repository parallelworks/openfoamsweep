/*---------------------------------------------------------------------------*\
| =========                 |                                                 |
| \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox           |
|  \\    /   O peration     | Version:  2.3.1                                 |
|   \\  /    A nd           | Web:      www.OpenFOAM.org                      |
|    \\/     M anipulation  |                                                 |
\*---------------------------------------------------------------------------*/
Build  : 2.3.1-bcfaaa7b8660
Exec   : foamToVTK -ascii
Date   : Feb 02 2015
Time   : 10:28:53
Host   : "ip-172-31-36-90"
PID    : 874
Case   : /home/compute/swiftwork/openfoamsweep-run002/jobs/7/sh-7cbu114m
nProcs : 1
sigFpe : Enabling floating point exception trapping (FOAM_SIGFPE).
fileModificationChecking : Monitoring run-time modified files using timeStampMaster
allowSystemOperations : Allowing user-supplied system call operations

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //
Create time

Create mesh for time = 0

Time: 0
    volScalarFields            : p nut k epsilon nuTilda
    volVectorFields            : U

    Internal  : "/home/compute/swiftwork/openfoamsweep-run002/jobs/7/sh-7cbu114m/VTK/sh-7cbu114m_0.vtk"
    Original cells:12225 points:25012   Additional cells:0  additional points:0

    Patch     : "/home/compute/swiftwork/openfoamsweep-run002/jobs/7/sh-7cbu114m/VTK/inlet/inlet_0.vtk"
    Patch     : "/home/compute/swiftwork/openfoamsweep-run002/jobs/7/sh-7cbu114m/VTK/outlet/outlet_0.vtk"
    Patch     : "/home/compute/swiftwork/openfoamsweep-run002/jobs/7/sh-7cbu114m/VTK/upperWall/upperWall_0.vtk"
    Patch     : "/home/compute/swiftwork/openfoamsweep-run002/jobs/7/sh-7cbu114m/VTK/lowerWall/lowerWall_0.vtk"
    Patch     : "/home/compute/swiftwork/openfoamsweep-run002/jobs/7/sh-7cbu114m/VTK/frontAndBack/frontAndBack_0.vtk"
Time: 50
    volScalarFields            : p nut k epsilon
    volVectorFields            : U

    Internal  : "/home/compute/swiftwork/openfoamsweep-run002/jobs/7/sh-7cbu114m/VTK/sh-7cbu114m_50.vtk"
    Patch     : "/home/compute/swiftwork/openfoamsweep-run002/jobs/7/sh-7cbu114m/VTK/inlet/inlet_50.vtk"
    Patch     : "/home/compute/swiftwork/openfoamsweep-run002/jobs/7/sh-7cbu114m/VTK/outlet/outlet_50.vtk"
    Patch     : "/home/compute/swiftwork/openfoamsweep-run002/jobs/7/sh-7cbu114m/VTK/upperWall/upperWall_50.vtk"
    Patch     : "/home/compute/swiftwork/openfoamsweep-run002/jobs/7/sh-7cbu114m/VTK/lowerWall/lowerWall_50.vtk"
    Patch     : "/home/compute/swiftwork/openfoamsweep-run002/jobs/7/sh-7cbu114m/VTK/frontAndBack/frontAndBack_50.vtk"
    surfScalarFields  : phi
Time: 100
    volScalarFields            : p nut k epsilon
    volVectorFields            : U

    Internal  : "/home/compute/swiftwork/openfoamsweep-run002/jobs/7/sh-7cbu114m/VTK/sh-7cbu114m_100.vtk"
    Patch     : "/home/compute/swiftwork/openfoamsweep-run002/jobs/7/sh-7cbu114m/VTK/inlet/inlet_100.vtk"
    Patch     : "/home/compute/swiftwork/openfoamsweep-run002/jobs/7/sh-7cbu114m/VTK/outlet/outlet_100.vtk"
    Patch     : "/home/compute/swiftwork/openfoamsweep-run002/jobs/7/sh-7cbu114m/VTK/upperWall/upperWall_100.vtk"
    Patch     : "/home/compute/swiftwork/openfoamsweep-run002/jobs/7/sh-7cbu114m/VTK/lowerWall/lowerWall_100.vtk"
    Patch     : "/home/compute/swiftwork/openfoamsweep-run002/jobs/7/sh-7cbu114m/VTK/frontAndBack/frontAndBack_100.vtk"
    surfScalarFields  : phi
Time: 150
    volScalarFields            : p nut k epsilon
    volVectorFields            : U

    Internal  : "/home/compute/swiftwork/openfoamsweep-run002/jobs/7/sh-7cbu114m/VTK/sh-7cbu114m_150.vtk"
    Patch     : "/home/compute/swiftwork/openfoamsweep-run002/jobs/7/sh-7cbu114m/VTK/inlet/inlet_150.vtk"
    Patch     : "/home/compute/swiftwork/openfoamsweep-run002/jobs/7/sh-7cbu114m/VTK/outlet/outlet_150.vtk"
    Patch     : "/home/compute/swiftwork/openfoamsweep-run002/jobs/7/sh-7cbu114m/VTK/upperWall/upperWall_150.vtk"
    Patch     : "/home/compute/swiftwork/openfoamsweep-run002/jobs/7/sh-7cbu114m/VTK/lowerWall/lowerWall_150.vtk"
    Patch     : "/home/compute/swiftwork/openfoamsweep-run002/jobs/7/sh-7cbu114m/VTK/frontAndBack/frontAndBack_150.vtk"
    surfScalarFields  : phi
Time: 200
    volScalarFields            : p nut k epsilon
    volVectorFields            : U

    Internal  : "/home/compute/swiftwork/openfoamsweep-run002/jobs/7/sh-7cbu114m/VTK/sh-7cbu114m_200.vtk"
    Patch     : "/home/compute/swiftwork/openfoamsweep-run002/jobs/7/sh-7cbu114m/VTK/inlet/inlet_200.vtk"
    Patch     : "/home/compute/swiftwork/openfoamsweep-run002/jobs/7/sh-7cbu114m/VTK/outlet/outlet_200.vtk"
    Patch     : "/home/compute/swiftwork/openfoamsweep-run002/jobs/7/sh-7cbu114m/VTK/upperWall/upperWall_200.vtk"
    Patch     : "/home/compute/swiftwork/openfoamsweep-run002/jobs/7/sh-7cbu114m/VTK/lowerWall/lowerWall_200.vtk"
    Patch     : "/home/compute/swiftwork/openfoamsweep-run002/jobs/7/sh-7cbu114m/VTK/frontAndBack/frontAndBack_200.vtk"
    surfScalarFields  : phi
End

