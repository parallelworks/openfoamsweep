#! /bin/bash
# set -x

usage="$0 --apps app_tar_URL --input inputfile --mesh meshtemplatefile --outall tarfile --params name value ..."

echo $usage

echo $@

while [ $# -gt 0 ]; do
  case $1 in
    --apps) apps=$2; shift 2;;
    --input) input=$2; shift 2;;
    --mesh) mesh=$2; shift 2;;
    --outall) outall=$2; shift 2;;
    --params) shift 1;
        while [ $# -gt 0 ] && ! echo $1 | grep -q -- -- ; do
          pname=$1
          pval=$2
          echo $pname
          echo $pval
          shift 2
          sed -i -e "s/@@$pname@@/$pval/g" $mesh 
        done
        ;;
    *) echo $usage 1>&2
     exit 1;;
  esac
done

tar xzf $PWD/$apps --strip-components=1
tar xzf $PWD/$input --strip-components=1

# put the mesh file into the polymesh directory
cp $mesh $PWD/constant/polyMesh/blockMeshDict

# run openfoam
. /opt/openfoam231/etc/bashrc
blockMesh > $PWD/results/blockMesh_log.txt
simpleFoam > $PWD/results/simpleFoam_log.txt
foamToVTK -ascii > $PWD/results/foamToVTK_log.txt

# move the result files
pwdsave=$PWD;cd $pwdsave/VTK;mv * $pwdsave/results;cd $pwdsave;

RC=$?

if [ $RC = 0 ]; then
  echo OpenFoam application completed: RC=0
else
  echo OpenFoam application failed: RC=$RC
fi

#tar zcf $outall *
tar zcf $outall results

exit $RC
