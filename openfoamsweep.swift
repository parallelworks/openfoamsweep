
# ----- TYPE DEFINITIONS

  type file;
  
  type param {
    string pname;
    string pvals;
  }
  
  type pval {
    string name;
    string[] values;
  }


# ----- APP DEFINITIONS

  app (file all) runOF ( file execute, file apps, file input, file mesh, string params[] )
  {
    sh @execute "--apps" @apps "--input" @input "--mesh" @mesh "--outall" @all "--params" params;
  }
  
  app (file all) runOFlocal ( file execute, file apps, file input, file mesh, string params[] )
  {
    shlocal @execute "--apps" @apps "--input" @input "--mesh" @mesh "--outall" @all "--params" params;
  }



# ----- EXTERNAL INPUTS
  
  string inputFile=@strcat(@arg("input","input"),".tgz");
  string appsFile=@strcat(@arg("apps","apps_worker"),".tgz");
  
  file mesh <single_file_mapper; file=@arg("mesh","blockMeshDict_template")>;
  file execute <single_file_mapper; file=@arg("execute","RunAndReduceOpenFoam.sh")>;
  file input <single_file_mapper; file=inputFile>;
  file apps <single_file_mapper; file=appsFile>;
  
  string outdir=@arg("outdir","results");
  string count=@arg("count","true");
  string runlocal=@arg("runlocal","true");
  
  param  pset[] = readData(@arg("sweep","test.sweep"));


# ----- INTERNAL FUNCTIONS

  (string[] r) extend(string[] a, string v,string n, int lastIndex) {
    foreach ov, i in a {r[i] = a[i];}
    string[] f;f[0]=n;f[1]=v;
    r[lastIndex]=@strjoin(f,",");
  }
  
  iterateOF(file ex1, file a1, file in1, file c, string[] parameters, string d, string l) {
    string ap=@strjoin(parameters,",");
    string[] apx=@strsplit(ap,",");
    string[] aval;
    foreach v, vn in parameters {
      string[] an=@strsplit(v,",");
      aval[vn]=an[1];
    }
    string fileid = @strcat("of+",@strjoin(aval,"+"));
    tracef("%s\n",fileid);
    file outall  <single_file_mapper; file=@strcat(d,"/",fileid,".tgz")>;  
    if (l=="false"){
      (outall) = runOF (ex1,a1,in1,c,apx);
    }else{
      (outall) = runOFlocal (ex1,a1,in1,c,apx);
    }
  }

  dynamicFor(pval[] vars, string[] values, int index, int len, file exc, file ac, file inc, file mc, string od, string rl) {
    if (index < len) {
      foreach v in vars[index].values {dynamicFor(vars, extend(values, v,vars[index].name, index), index + 1, len, exc, ac, inc, mc, od, rl);}
    }else {iterateOF(exc,ac,inc,mc,values,od,rl);}
  }

  (int sz) countAllRecursive(pval[] pval, int index) {
    if (index < 0) {sz = 1;}
    else {sz = @length(pval[index].values) * countAllRecursive(pval, index - 1);}
  }

  (int sz) countAll(pval[] pval) {
    if (@length(pval) == 0) {sz = 0;}
    else {sz = countAllRecursive(pval, @length(pval) - 1);}
  }


# ----- PARAMETER SETUP & LAUNCH
  
  string[] empty;
  pval[] pvals;

  foreach p, pn in pset {
      string val[]=@strsplit(p.pvals,",");
      pvals[pn].name=p.pname;
      foreach v, vn in val {
        pvals[pn].values[vn]=v;
      }
  }
  
  int runs=countAll(pvals);
  tracef("%i Runs in Simulation\n\n",runs);
  
  if (count=="false"){
      dynamicFor(pvals, empty, 0, @length(pvals), execute, apps, input, mesh, outdir, runlocal);
  }
